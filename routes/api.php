<?php

use Illuminate\Http\Request;

Route::prefix('auth')->group(function() {
    Route::post('login', 'AuthenticationController@login');
    Route::post('signup', 'AuthenticationController@signup');

    Route::middleware('auth:api')->group(function() {
        Route::get('logout', 'AuthenticationController@logout');
        Route::get('user', 'AuthenticationController@user');
    });
});

